#include <iostream>
#include <cstdlib>
#include <ctime>
#include <limits>
#include <string>

using namespace std;

int main(int argc, char **argv)
{
    long unsigned int r, n, max, i;
    int line, max_line;
    string output, param;

    if(argc < 2) {
        cout << "Uso: ./generator how_many_numbers > file" << endl;
        return 1;
    }

    param = argv[1];
    n = stol(param);
    i = 0;
    line = 0;
    max_line = 10;
    max = numeric_limits<long unsigned int>::max();
    output = "";
	srand((int)time(0));
	while(i++ < n) {
		r = (rand() % max) + 1;
        output += to_string(r) + " ";
        if(line == max_line) {
            output += "\n";
            line = 0;
        }
        line++;
	}

    cout << output;
    
	return 0;
}