#!/bin/bash

Files=("nums1" "nums2" "nums")
Algs=("heap" "merge" "quick" "insertion")
for f in ${Files[@]}; do
    for alg in ${Algs[@]}; do
        for i in 1 2 3 4 5; do
            result="result/perf_"$alg"_"$i"_"$f".txt"
            perf stat -e task-clock,cycles,instructions,cache-references,cache-misses,branch,branch-misses ./bin/caching -alg $alg -num $f".txt" 2> $result
        done
    done
done

