/**
 *  Mix codes from https://www.geeksforgeeks.org/cpp-program-for-quicksort/ and
 *  https://appdividend.com/2019/05/02/quick-sort-in-c-tutorial-with-example-c-quick-sort-program/
 * 
 */
#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <utility>

using namespace std;

long unsigned int partition (long unsigned int arr[], long unsigned int low, long unsigned int high);
void quickSort(long unsigned int arr[], long unsigned int low, long unsigned int high);

#endif