#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
using namespace std;

inline long unsigned int min(long unsigned int x, long unsigned int y) { return (x < y) ? x : y; }
void printArr(long unsigned int arr[], long unsigned int s);

#endif