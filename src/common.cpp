#include "common.h"

void printArr(long unsigned int arr[], long unsigned int s)
{
    long unsigned int i;
    string out;

    out = "";
    for(i = 0; i < s; i++) {
        out += to_string(arr[i]) + " ";
    }
    cout << out << endl;
}