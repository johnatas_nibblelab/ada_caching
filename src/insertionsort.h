/**
 * Code from https://www.tutorialspoint.com/cplusplus-program-to-implement-insertion-sort
 * 
 */
#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

using namespace std;

void insertionSort(long unsigned int arr[], long unsigned int n);

#endif