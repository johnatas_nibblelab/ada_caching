#include "quicksort.h"

/* This function takes last element as pivot, places
   the pivot element at its correct position in sorted
    array, and places all smaller (smaller than pivot)
   to left of pivot and all greater elements to right
   of pivot */
long unsigned int partition (long unsigned int arr[], long unsigned int low, long unsigned int high)
{
    long unsigned int i, j;
    long unsigned int pivot = arr[high];    // pivot
    j = low;
    
    for(i = low; i < high; i++)
    {
        if(arr[i] <= pivot)
        {
            swap(arr[i], arr[j]);
            j++;
        }
    }
    
    swap(arr[j], arr[high]);
    return j;
}

/* The main function that implements QuickSort
 arr[] --> Array to be sorted,
  low  --> Starting index,
  high  --> Ending index */
void quickSort(long unsigned int arr[], long unsigned int low, long unsigned int high)
{
    long unsigned int pi, n_low, n_high;

    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        pi = partition(arr, low, high);

        // ajuste por conta dos unsigned
        n_low = pi + 1;
        n_high = (pi == 0) ? 0 : (pi - 1);
 
        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, n_high);
        quickSort(arr, n_low, high);
    }
}