/**
 * Code from https://www.geeksforgeeks.org/cpp-program-for-heap-sort/
 * 
 */
#ifndef HEAPSORT_H
#define HEAPSORT_H

#include <utility>

using namespace std;

void heapify(long unsigned int arr[], long unsigned int n, long unsigned int i);
void heapSort(long unsigned int arr[], long unsigned int n);

#endif