#include <iostream>
#include <string>
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <istream>
#include <sstream>
#include <iterator>
#include <cstdlib>

#include "heapsort.h"
#include "insertionsort.h"
#include "mergesort.h"
#include "quicksort.h"

using namespace std;

namespace fs = std::filesystem;

inline string trim(string& str)
{
    str.erase(0, str.find_first_not_of(" "));  
    str.erase(str.find_last_not_of(" ")+1); 
    return str;
}

void readData(string file, long unsigned int** result, long unsigned int* size)
{
    vector<long unsigned int> temp;
    string line, l;

    ifstream f(file);
    temp.clear();
    for (; getline(f, line);) 
    {
        l = trim(line);
        if(l.empty()) {
            continue;
        }

        istringstream iss(l);
        vector<string> results(istream_iterator<string>{iss}, istream_iterator<string>());
        for (vector<string>::const_iterator i = results.begin(); i != results.end(); ++i) {
            auto v = stol(*i); 
            temp.push_back(v);
        }
    }

    (*result) = (long unsigned int*) malloc(sizeof(long unsigned int)*temp.size());
    copy(temp.begin(), temp.end(), (*result));
    (*size) = temp.size();
}

int main(int argc, char **argv)
{
    long unsigned int* arr;
    long unsigned int s;
    int i;
    string param, alg, input;
    bool mask[2] = {false,false};
    string algs[] = {"heap","insertion","merge","quick"};
    string* alg_f;

    if(argc < 3) {
        cout << "Uso: ./caching -alg [heap,insertion,merge,quick] -num input_file" << endl;
        return 1;
    }

    for(i = 1; i < argc; ++i) {
        param = argv[i];
        if(mask[0]) {
            alg = param;
            mask[0] = false;
        }
        if(mask[1]) {
            input = param;
            mask[1] = false;
        }
        mask[0] = (param == "-alg");
        mask[1] = (param == "-num");
    }

    alg_f = find(begin(algs), end(algs), alg);
    if(alg_f == end(algs)) {
        cout << "É necessário fornecer um algoritmo de ordenação suportado: heap,insertion,merge,quick." << endl << 
                "Uso: ./caching -alg [heap,insertion,merge,quick] -num input_file" << endl;
        return 1;
    }

    if(input.empty())
    {
        cout << "É necessário fornecer um arquivo com os números a serem ordenados" << endl << 
                "Uso: ./caching -alg [heap,insertion,merge,quick] -num input_file" << endl;
        return 1;
    }

    if(!fs::exists(input))
    {
        cout << "O arquivo fornecido " << input << " não existe!" << endl << 
                "Uso: ./caching -alg [heap,insertion,merge,quick] -num input_file" << endl;
        return 1;
    }

    readData(input, &arr, &s);

#ifdef DEBUG
    cout << "Antes: " << endl;
    printArr(arr, s);
#endif

    if(alg == algs[0]) {
        // heapsort
        heapSort(arr,s);
    }
    else if(alg == algs[1]) {
        // insertion
        insertionSort(arr,s);
    }
    else if(alg == algs[2]) {
        // merge
        mergeSort(arr,s);
    }
    else if(alg == algs[3]) {
        // quick
        quickSort(arr, 0, s-1);
    }

#ifdef DEBUG
    cout << "Depois: " << endl;
    printArr(arr, s);
#endif
    
    return 0;
}