#include "insertionsort.h"

void insertionSort(long unsigned int arr[], long unsigned int n)
{
    long unsigned int i, key, j;
    for (i = 1; i < n; i++)
    {
        key = arr[i];
        j = i;
 
        /* Move elements of arr[0..i-1], that are
        greater than key, to one position ahead
        of their current position */
        while (j > 0 && arr[j-1] > key)
        {
            arr[j] = arr[j-1];
            j--;
        }
        arr[j] = key;
    }
}