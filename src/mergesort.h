/**
 * Code from http://www.cplusplus.com/forum/beginner/165054/
 * 
 */
#ifndef MERGESORT_H
#define MERGESORT_H

#include "common.h"

void merge(long unsigned int a[], long unsigned int b[], long unsigned int size, long unsigned int left, long unsigned int mid);
void mergeSort(long unsigned int arr[], long unsigned int n);

#endif