#include "mergesort.h"

void merge(long unsigned int a[], long unsigned int b[], long unsigned int size, long unsigned int left, long unsigned int mid)
{
    long unsigned int right, i, j, k;

    right = mid + mid - left;
	if (right > size)
    {
        right = size;
    }
	i = left;
	j = mid;
	k = left;
	while (i < mid && j < right)
	{
		if (a[i] < a[j]) 
        {
            b[k++] = a[i++];
        }
		else
        {
            b[k++] = a[j++];
        }	
	}
	while (i < mid)
    {
        b[k++] = a[i++];
    }
	while (j < right)
    {
        b[k++] = a[j++];
    }
	for (i = left; i < right; ++i)
    {
        a[i] = b[i];
    }
}

void mergeSort(long unsigned int arr[], long unsigned int n)
{
    long unsigned int subsize, left, mid;
    long unsigned int* b;

    b = new long unsigned int[n]; // must be dynamic: https://stackoverflow.com/questions/60024103/merge-sort-segmentation-fault-only-on-large-arrays
	for (subsize = 1; subsize < n; subsize *= 2) 
    {
        for (left = 0, mid = subsize; mid < n; left = mid + subsize, mid = left + subsize)
        {
            merge(arr, b, n, left, mid);
        }
    }
    delete b;
}

