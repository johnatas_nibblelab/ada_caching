CC := g++
CFLAGS := -Wsign-compare -Wunused -std=c++17
FLAGS = 
LFLAGS = -lstdc++fs
AUX		:= ./aux
BIN     := ./bin
OBJ     := ./obj
SRC     := ./src
SOURCES := $(wildcard $(SRC)/*.cpp)
TARGETS := $(patsubst $(SRC)/%.cpp,$(OBJ)/%.o,$(SOURCES))
EXE     := $(BIN)/caching 

CCV = $(shell $(CC) -dumpversion | cut -f1 -d.)
ifeq ($(shell expr $(CCV) \< 8), 1)
define ERRMSG
*****************************************************************
ERRO: Versão do g++ sem suporte a libfilesystem. 
Use g++ versão 8 ou superior
*****************************************************************
endef
$(error $(ERRMSG))
endif

ifeq ($(debug),1)
	FLAGS += -D "DEBUG"
endif

ifeq ($(profile),1)
	CFLAGS += -g
endif

.PHONY: all

all: $(EXE)
	$(CC) $(CFLAGS) -o $(BIN)/generator $(AUX)/generator.cpp

$(EXE): $(TARGETS) | $(BIN)
	$(CC) $(LFLAGS) $^ -o $@

$(OBJ)/%.o: $(SRC)/%.cpp | $(OBJ)
	$(CC) $(CFLAGS) $(FLAGS) -c $< -o $@

$(BIN):
	test ! -d $(BIN) && mkdir $(BIN)

$(OBJ): 
	test ! -d $(OBJ) && mkdir $(OBJ)

clean:
	rm -rf bin/*
	rm -rf obj/*
